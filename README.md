# Password Self Service

The Password Self Service app is designed to allow students of the Sally McDonnell Barksdale Honors College to change their own passwords (a la "Forgot my password") using WebID as their security challenge. This will reduce the reliance on Honors College staff when a student forgets their password and gives them the freedom to change it.

The app itself is built with:  
* [Slim PHP web framework](https://www.slimframework.com)  
* [Shibboleth](https://www.shibboleth.net/)
* [ADLDAP2](https://github.com/Adldap2/Adldap2/)
* [PHPMailer](https://github.com/PHPMailer/PHPMailer)


## Installation

0. Prerequisites

This a app requires that you have a functional and authenticating Shibboleth server established within the University. This can be tested by visiting your servers' Shib instance at ```https://<server>/Shibboleth.sso/Login```. This should redirect you to the login page with the University. After a success login, visiting ```https://<server>/Shibboleth.sso/Session``` should so if you have a valid shib session or not. 

1. Apache

    1. Create an Apache conf in __conf.d__
```
sudo touch /etc/httpd/conf.d/php-shib-example.conf
```
    2. Edit the conf to include the following
```
<Location /password-self-service>
   AuthType Shibboleth
   ShibRequestSetting requireSession false
   Require shibboleth
 </Location>
 <Location /password-self-service/login>
   AuthType shibboleth
   ShibRequestSetting requireSession 1
   ShibUseEnvironment On
   require shib-session
 </Location>
 ```

2. Get Composer Libraries

With the included ```composer.json``` just run ```composer update```

3. LDAP and Mailer Settings files

Both files ```ldap_settings.php``` and ```phpmailer_settings.php``` are meant to be placed outside of the public-facing web directory. The ```ldap_settings.php``` will need to contain information about how ADLDAP can connect to your Domain, see example below.

**Note:** to be able to set password via ADLDAP, you will need to setup SSL certificates between your server and your domain.

Examples
```
ldap_settings.php
<?php
$ldap_config = [
  'hosts'    => ['<ip address>,<or hostname>'],
  'base_dn'  => 'dc=dc,dc=example,dc=edu',
  'username' => 'username@dc.example.edu',
  'password' => '<hopefully a long passwor',
  'use_tls'  => TRUE,
];
```

```
phpmailer_settings.php
<?php
$smtp_settings = [
  'server' => '<smtp server>',
  'username' => '<user or email, depending>',
  'password' => '<password>',
];
```


## ToDo

* Logging -> Need to log the users and when they're passwords are changed. This will probably be useful for some auditing down the road.
* Sessions/No-Refresh -> Need to add a mechanism that will prevent multiple, successive password resets. I see a case where a user refreshes the page after they log in to the lab
* Templates -> Need some templates for the few pages that are going to be used

## Special Thanks
Special thanks to DSG and the Sally McDonnell Barksdale Honors College for allowing me time to work on this problem.