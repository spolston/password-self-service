<?php

require 'vendor/autoload.php';
require '../../ldap-config2.php';
require '../../phpmailer-setting.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$config = [
  // Extending error messages onto screen
  // 'settings' => [
  //     'displayErrorDetails' => true,
  // ],
];

// Instantiate App
$app = new \Slim\App($config);

// Set up Active Directory Connection
$ad = new \Adldap\Adldap();
$ad->addProvider($ldap_config);
try {
  $provider = $ad->connect();

  // Great, we're connected!
  // print "connected";
} catch (\Adldap\Auth\BindException $e) {
  // Failed to connect.
  print "not connected";
  print $e;
}

// Start a session
session_start();

// Set up Templating
$container = $app->getContainer();
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('assets/templates', []);
    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new Slim\Views\TwigExtension($router, $uri));

    return $view;
};

// Set up Logging via monolog
$container['logger'] = function ($container) {
  $logger = new Monolog\Logger("password-self-service");
  $logger->pushProcessor(new Monolog\Processor\UidProcessor());
  $logger->pushHandler(new Monolog\Handler\StreamHandler("../../logs/password-self-service.log", \Monolog\Logger::DEBUG));
  return $logger;
};

// Routes
$app->get('/', function (Request $request, Response $response) {
  global $provider;
  global $smtp_settings;

  $time_check = true;
  if ( isset($_SESSION['chronos']) && $_SESSION['chronos'] + 1800 > time() ){
    $time_check = false;
  }

  if ( isset($_SERVER['uid']) && $time_check ){
    $webID = $_SERVER['uid'];
    $eduPersonAffiliation = explode(", ", $_SERVER['eduPersonAffiliation']);
    $needle = "student";
    $fneedle = "faculty";
    $sneedle = "staff";

    if ( in_array($needle, $eduPersonAffiliation) ){
      try{
        $user = $provider->search()->findBy('samaccountname', $webID);
      } catch (\Adldap\Models\ModelNotFoundException $e) {
        // Record wasn't found!
        $this->logger->error("USER: {$_SERVER['uid']}\tHOCO Account?:\t\t❌");
        return "I'm sorry, but we couldn't find your Honors College Account in our system.";
      }
      $fname = $user->getDisplayName();
      $email = $user->getEmail();
      if ( !isset( $email ) ){
        if ( strpos($user["distinguishedname"][0], "Faculty") !== false){
          // print "is a faculty";
          $email = $_SERVER["uid"] . "@olemiss.edu";
        } else {
          // print "is not a faculty";
          $email = $_SERVER["uid"] . "@go.olemiss.edu";
        }
      }
      $temppass = generate_password();
      // print "Your name is ".$fname." and you email is ".$email." and your new pass will be ".generate_password();
      try{
        $user->setPassword($temppass);
        $user->setEnableForcePasswordChange();
        $user->save();
        $this->logger->info("USER: {$_SERVER['uid']}\tPassword Change?:\t✔️");
      } catch (Exception $e) {
        // return "Couldn't set password or force password change/n/n".$e;
        $this->logger->error("USER: {$_SERVER['uid']}\tPassword Change?:\t❌\n{$e}");
        return $this->view->render($response, 'error.twig');
      }
      $email_res = send_an_email($email, $temppass, $smtp_settings);
      if ($email_res) {
        // return "Password was changed and an email was sent";
        $_SESSION['chronos'] = time();
        $this->logger->info("USER: {$_SERVER['uid']}\tEmailed?:\t\t✔️");
        return $this->view->render($response, 'email.twig');
      } else {
        // return "There was a problem sending you an email. Your password is " . $temppass . "/n/n".$e;
        $this->logger->error("USER: {$_SERVER['uid']}\tEmailed?:\t\t❌\n{$email_res}");    
        return $this->view->render($response, 'error.twig');
      }

    } elseif ( in_array($fneedle, $eduPersonAffiliation) || in_array($sneedle, $eduPersonAffiliation) ){
      try{
        $user = $provider->search()->findBy('samaccountname', $webID);
      } catch (\Adldap\Models\ModelNotFoundException $e) {
        // Record wasn't found!
        $this->logger->error("USER: {$_SERVER['uid']}\tHOCO Account?:\t\t❌");
        return "I'm sorry, but we couldn't find your Honors College Account in our system.";
      }
      $fname = $user->getDisplayName();
      $email = $user->getEmail();
      if ( !isset( $email ) ){
        $email = $_SERVER["uid"] . "@olemiss.edu";
      }
      $temppass = "olemiss2019";
      try{
        $user->setPassword($temppass);
        $user->setEnableForcePasswordChange();
        $user->save();
        $this->logger->info("USER: {$_SERVER['uid']}\tPassword Change?:\t✔️");
      } catch (Exception $e) {
        // return "Couldn't set password or force password change/n/n".$e;
        $this->logger->error("USER: {$_SERVER['uid']}\tPassword Change?:\t❌\n{$e}");
        return $this->view->render($response, 'error.twig');
      }
      return $this->view->render($response, 'fac.twig');
    }

  } else {
    // return $response->withStatus(302)->withHeader('Location', 'login');
    return $this->view->render($response, 'landing.twig');
  }
});

$app->get('/login', function (Request $request, Response $response) {
  $this->logger->info("USER: {$_SERVER['uid']}\tWEBID:\t\t\t✔️");
  return $response->withStatus(302)->withHeader('Location', '/password-self-service/');
});

//Debugging
$app->get('/debug', function(Request $request, Response $response) {
  // $body = $response->getBody();
  // $newRes = $body->withJson(vardump($_SESSION));
  return $response->write(generate_password());
});

// Functions
function generate_password(){
  $adjectives = file("assets/data/adjectives.txt");
  $animals = file("assets/data/animals.txt");
  $num = rand(0, 99);
  return trim($adjectives[array_rand($adjectives)]).trim($animals[array_rand($animals)]).$num;
}

function send_an_email($email, $temppass, $smtp_settings){
  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
  try {
    //Server settings
    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $smtp_settings["server"];  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $smtp_settings["username"];                 // SMTP username
    $mail->Password = $smtp_settings["password"];                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('smbhc-noreply@olemiss.edu', 'SMBHC Password Reset');
    $mail->addAddress($email);               // Name is optional
    $mail->addReplyTo('spolston@olemiss.edu', 'Scotty Polston');

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Honors College Password Reset';
    $mail->Body    = "A request has been made to reset your Honors College password. This request was processed and your temporary password is<br><br>
    {$temppass}
    <br /><br />
    This password will only work the first time you log in. You will be prompted to change this when you use it. If you have additional problems, or if you did not request a password reset, please reply to this email.
    ";
    $mail->AltBody = "A request has been made to reset your Honors College password. This request was processed and your temporary password is

    {$temppass}

    This password will only work the first time you log in. You will be prompted to change this when you use it. If you have additional problems, or if you did not request a password reset, please reply to this email.
    ";

    $mail->send();
    return true;
  } catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
  }
}

// Slim Requirement
$app->run();
